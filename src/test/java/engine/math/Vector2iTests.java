package engine.math;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Vecteur de 2 entiers")
public class Vector2iTests {

    @Test
    @DisplayName("Constructeur par défaut")
    public void testDefaultContructor() {
        Vector2i v = new Vector2i();
        assertEquals(v.x, 0);
        assertEquals(v.y, 0);
    }

    @Test
    @DisplayName("Constructeur prendant un autre vecteur")
    public void testConstructorWithPosition() {
        Vector2i v1 = new Vector2i(2, 4);
        Vector2i v2 = new Vector2i(v1);
        assertEquals(v1.x, v2.x);
        assertEquals(v1.y, v2.y);
    }

    @Test
    @DisplayName("Constructeur avec coordonnées")
    public void testContructorWithCoordinates() {
        Vector2i v = new Vector2i(2, 3);
        assertEquals(v.x, 2);
        assertEquals(v.y, 3);
    }

    @DisplayName("Egalités")
    @Nested
    class Equality {

        Vector2i v1;

        @BeforeEach
        void createNewVector() {
            v1 = new Vector2i(2, 3);
        }

        @Test
        @DisplayName("Entre 2 vecteur")
        public void testEquals() {
            Vector2i v2 = new Vector2i(2, 3);
            assertEquals(v1, v2);
        }

        @Test
        @DisplayName("Avec coordonnée X différente")
        public void testXNotEquals() {
            Vector2i v2 = new Vector2i(3, 3);
            assertNotEquals(v1, v2);
        }

        @Test
        @DisplayName("Avec coordonnée Y différente")
        public void testYNotEquals() {
            Vector2i v2 = new Vector2i(2, 4);
            assertNotEquals(v1, v2);
        }

        @Test
        @DisplayName("Avec coordonnées X et Y différentes")
        public void testXYNotEquals() {
            Vector2i v2 = new Vector2i(3, 4);
            assertNotEquals(v1, v2);
        }

        @Test
        @DisplayName("Avec un autre objet")
        public void testEqualsWithoutVector() {
            assertNotEquals(v1, new Object());
        }

    }

    @Test
    @DisplayName("Formattage en chaîne de caractères")
    public void testToString() {
        Vector2i v = new Vector2i(2, 3);
        assertTrue(v.toString().contains("C"));
        assertTrue(v.toString().contains("4"));
    }

}
