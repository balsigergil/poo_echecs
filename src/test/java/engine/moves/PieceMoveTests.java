package engine.moves;

import chess.PlayerColor;
import engine.core.Player;
import engine.math.Vector2i;
import engine.pieces.Piece;
import engine.pieces.Rook;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

public class PieceMoveTests {

    private Piece piece;
    private PieceMove pieceMove;

    @BeforeEach
    public void beforeApply() {
        Vector2i newPosition = new Vector2i(2, 0);
        piece = Mockito.spy(new Rook(0, 0, new Player(PlayerColor.WHITE)));
        doReturn(true).when(piece).move(newPosition);
        pieceMove = spy(new PieceMove(piece, newPosition));
    }

    @Test
    public void testApplyValidate() {
        assertTrue(pieceMove.apply());
    }

    @Test
    public void testApplyNotValidate() {
        piece.setPosition(new Vector2i(2, 0));
        assertFalse(pieceMove.apply());
    }

}
