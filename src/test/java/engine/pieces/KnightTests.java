package engine.pieces;

import chess.PieceType;
import chess.PlayerColor;
import engine.core.Player;
import engine.math.Vector2i;
import engine.utils.PiecesUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doReturn;

public class KnightTests {

    Player pl1;
    Player pl2;
    Knight knight;

    @BeforeEach
    void setupBoard() {
        Pieces.removeAllPieces();
        pl1 = new Player(PlayerColor.WHITE);
        pl2 = new Player(PlayerColor.BLACK);
        pl1.setup();
        pl2.setup();
        knight = (Knight) Pieces.getPieceAt(new Vector2i(1, 0));
        knight.setPosition(new Vector2i(3, 3));

        for (Piece p : Pieces.getAllPiecesOfType(PieceType.PAWN)) {
            Pieces.remove(p);
        }
    }

    @DisplayName("Correct moves")
    @ParameterizedTest(name = "Correct move {index} from (3, 3) to ({0}, {1})")
    @CsvSource({
            "2, 5", "4, 5",
            "5, 4", "5, 2",
            "4, 1", "2, 1",
            "1, 2", "1, 4"
    })
    public void testMoveUpLeft(int x, int y) {
        PiecesUtils.fillBoard(pl2);
        assertTrue(knight.checkMove(new Vector2i(x, y)));
    }

    @DisplayName("Incorrect moves")
    @ParameterizedTest(name = "Incorrect move {index} from (3, 3) to ({0}, {1})")
    @CsvSource({
            "3, 4", "3, 5",
            "4, 3", "5, 3",
            "3, 2", "3, 1",
            "2, 3", "1, 3",
            "4, 4", "5, 5",
            "4, 2", "5, 1",
            "2, 2", "1, 1",
            "2, 4", "1, 5"
    })
    public void testMoveUp(int x, int y) {
        assertFalse(knight.checkMove(new Vector2i(x, y)));
    }

    @DisplayName("Piece cause player to be in check if moved")
    @Test
    public void testInCheck() {
        Vector2i newPos = new Vector2i(2, 5);
        Knight spy = Mockito.spy(knight);
        doReturn(false).when(spy).playerWontBeInCheck(newPos, null);
        assertFalse(spy.checkMove(newPos));
    }

}
