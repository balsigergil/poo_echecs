package engine.pieces;

import chess.PlayerColor;
import engine.core.Player;
import engine.math.Vector2i;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class PiecesTests {

    Player pl1;
    Player pl2;

    @BeforeEach
    void setup() {
        Pieces.removeAllPieces();
    }

    @DisplayName("Add new piece")
    @Test
    public void testAdd() {
        assertEquals(0, Pieces.getPieceCount());
        new Rook(0, 0, new Player(PlayerColor.WHITE));
        assertEquals(1, Pieces.getPieceCount());
    }

    @DisplayName("Remove one piece")
    @Test
    public void testRemove() {
        Piece piece = new Rook(0, 0, new Player(PlayerColor.WHITE));
        assertEquals(1, Pieces.getPieceCount());
        Pieces.remove(piece);
        assertEquals(0, Pieces.getPieceCount());
    }

    @Test
    public void testGetPieceAt() {
        Piece piece = new Rook(0, 0, new Player(PlayerColor.WHITE));
        Pieces.add(piece);
        assertEquals(piece, Pieces.getPieceAt(new Vector2i(0, 0)));
    }

    @Test
    public void testGetPieceAtNull() {
        assertNull(Pieces.getPieceAt(new Vector2i(0, 1)));
    }

    private void initPlayers() {
        pl1 = new Player(PlayerColor.WHITE);
        pl2 = new Player(PlayerColor.BLACK);
        pl1.setup();
        pl2.setup();
    }

    @Test
    public void testGetAllPiecesFrom() {
        initPlayers();
        ArrayList<Piece> pieces = Pieces.getAllPiecesFrom(pl1);
        for (Piece p : pieces) {
            assertEquals(pl1, p.getPlayer());
        }
        assertEquals(16, pieces.size());
    }

    @DisplayName("emptyCell() return false")
    @Test
    public void testEmptyCellFalse() {
        initPlayers();
        assertFalse(Pieces.emptyCell(new Vector2i(0, 0)));
    }

    @DisplayName("emptyCell() return true")
    @Test
    public void testEmptyCellTrue() {
        initPlayers();
        assertTrue(Pieces.emptyCell(new Vector2i(5, 5)));
    }

    @DisplayName("Remove piece at position")
    @Test
    public void testRemovePieceAt() {
        initPlayers();
        Pieces.removePieceAt(new Vector2i(0, 0));
        assertNull(Pieces.getPieceAt(new Vector2i(0, 0)));
    }

    @DisplayName("Remove all pieces")
    @Test
    public void testRemoveAllPieces() {
        initPlayers();
        assertEquals(32, Pieces.getPieceCount());
        Pieces.removeAllPieces();
        assertEquals(0, Pieces.getPieceCount());
    }

}
