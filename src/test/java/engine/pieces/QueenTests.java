package engine.pieces;

import chess.PieceType;
import chess.PlayerColor;
import engine.core.Player;
import engine.math.Vector2i;
import engine.utils.PiecesUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doReturn;

public class QueenTests {

    Player pl1;
    Player pl2;
    Queen queen;

    @BeforeEach
    void setupBoard() {
        Pieces.removeAllPieces();
        pl1 = new Player(PlayerColor.WHITE);
        pl2 = new Player(PlayerColor.BLACK);
        pl1.setup();
        pl2.setup();
        queen = (Queen) Pieces.getPieceAt(new Vector2i(3, 0));
        queen.setPosition(new Vector2i(3, 3));

        for (Piece p : Pieces.getAllPiecesOfType(PieceType.PAWN)) {
            Pieces.remove(p);
        }
    }

    @DisplayName("Horizontal and vertical moves")
    @ParameterizedTest(name = "Correct move straight {index} from (3, 3) to ({0}, {1})")
    @CsvSource({
            "3, 4", "3, 6",
            "4, 3", "5, 3",
            "3, 2", "3, 1",
            "2, 3", "0, 3"
    })
    public void testMoveHorizontalVertical(int x, int y) {
        assertTrue(queen.checkMove(new Vector2i(x, y)));
    }

    @DisplayName("Diagonal moves")
    @ParameterizedTest(name = "Correct move diagonal {index} from (3, 3) to ({0}, {1})")
    @CsvSource({
            "4, 4", "5, 5",
            "4, 2", "5, 1",
            "2, 2", "1, 1",
            "2, 4", "1, 5",
    })
    public void testMoveDiagonal(int x, int y) {
        assertTrue(queen.checkMove(new Vector2i(x, y)));
    }

    @DisplayName("Move without empty line of sight")
    @ParameterizedTest(name = "Move without empty line of sight {index} from (3, 3) to ({0}, {1})")
    @CsvSource({
            "3, 5", "5, 3", "3, 1", "1, 3",
            "5, 5", "5, 1", "1, 1", "1, 5"
    })
    public void testMoveNotEmptyLineOfSight(int x, int y) {
        PiecesUtils.fillBoard(pl2);
        assertFalse(queen.checkMove(new Vector2i(x, y)));
    }

    @DisplayName("Piece cause player to be in check if moved")
    @Test
    public void testInCheck() {
        Vector2i newPos = new Vector2i(3, 4);
        Queen spy = Mockito.spy(queen);
        doReturn(false).when(spy).playerWontBeInCheck(newPos, null);
        assertFalse(spy.checkMove(newPos));
    }
}
