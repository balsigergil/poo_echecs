package engine.pieces;

import chess.PieceType;
import chess.PlayerColor;
import engine.core.Chessboard;
import engine.core.Player;
import engine.math.Vector2i;
import engine.moves.MoveCapability;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class PieceTests {

    private Rook whiteRook;
    private Rook blackRook;

    @BeforeEach
    void setupBoard() {
        Pieces.removeAllPieces();
        Chessboard.getInstance().setupBoard();
        whiteRook = (Rook) Pieces.getPieceAt(new Vector2i(0, 0));
        blackRook = (Rook) Pieces.getPieceAt(new Vector2i(0, 7));
    }

    @DisplayName("Constructor with illegal positions")
    @Test
    public void testConstructor() {
        Player plTest = new Player(PlayerColor.WHITE);
        assertThrows(IllegalArgumentException.class, () -> {
            new Rook(-1, 3, plTest);
        });
        assertThrows(IllegalArgumentException.class, () -> {
            new Rook(3, 9, plTest);
        });
    }

    @DisplayName("Move won't cause the player to be in check")
    @Test
    public void testWontBeInCheck() {
        assertTrue(whiteRook.playerWontBeInCheck(new Vector2i(0, 2), null));
    }

    @DisplayName("Move will cause the player to be in check")
    @Test
    public void testWillBeInCheck() {
        Pieces.removePieceAt(new Vector2i(3, 0));
        Pieces.removePieceAt(new Vector2i(2, 0));
        whiteRook.setPosition(new Vector2i(3, 0));
        blackRook.setPosition(new Vector2i(2, 0));
        assertFalse(whiteRook.playerWontBeInCheck(new Vector2i(3, 3), null));
    }

    @DisplayName("Move method")
    @Test
    public void testMove() {
        blackRook.setPosition(new Vector2i(0, 2));
        Knight k = (Knight) Pieces.getPieceAt(new Vector2i(1, 0));
        assertNotNull(k);
        assertTrue(k.isFirstMove());
        k.move(new Vector2i(0, 2));
        Piece k2 = Pieces.getPieceAt(new Vector2i(0, 2));
        Piece shouldBeNull = Pieces.getPieceAt(new Vector2i(1, 0));
        assertNotNull(k2);
        assertEquals(k, k2);
        assertNull(shouldBeNull);
        assertFalse(k.isFirstMove());
        assertEquals(31, Pieces.getPieceCount());
    }

    @DisplayName("Testing move capabilities")
    @Test
    public void testCanMoveTo() {
        MoveCapability capability1 = mock(MoveCapability.class);
        MoveCapability capability2 = mock(MoveCapability.class);
        MoveCapability capability3 = mock(MoveCapability.class);

        Piece p = new Piece(3, 3, PieceType.QUEEN, Chessboard.getInstance().getPlayerWhite()) {};

        p.addCapability(capability1);
        p.addCapability(capability2);
        p.addCapability(capability3);

        Vector2i coords = new Vector2i(4, 3);
        p.checkMove(coords);

        // On test que toutes les capacitées sont vérifiées
        verify(capability1).verify(p, coords);
        verify(capability2).verify(p, coords);
        verify(capability3).verify(p, coords);
    }

    @DisplayName("Test set piece position")
    @Test
    public void testSetPosition() {
        Knight k = (Knight) Pieces.getPieceAt(new Vector2i(1, 0));
        assertNotNull(k);
        assertTrue(k.isFirstMove());
        k.setPosition(new Vector2i(0, 2));
        Knight k2 = (Knight) Pieces.getPieceAt(new Vector2i(0, 2));
        Knight shouldBeNull = (Knight) Pieces.getPieceAt(new Vector2i(1, 0));
        assertNotNull(k2);
        assertNull(shouldBeNull);
        assertEquals(k, k2);
        assertTrue(k.isFirstMove());
    }

    @Test
    public void testGetPosition() {
        Vector2i pos = new Vector2i(4, 0);
        Piece p = Pieces.getPieceAt(pos);
        assertEquals(p.getPosition(), pos);
    }

    @Test
    public void testEmptyLineOfSight() {
        assertFalse(whiteRook.emptyLineOfSight(new Vector2i(0, 7)));
        assertFalse(whiteRook.emptyLineOfSight(new Vector2i(3, 0)));
        whiteRook.setPosition(new Vector2i(0, 3));
        assertTrue(whiteRook.emptyLineOfSight(new Vector2i(0, 6)));
        assertTrue(whiteRook.emptyLineOfSight(new Vector2i(3, 3)));
        assertTrue(whiteRook.emptyLineOfSight(new Vector2i(3, 6)));
        assertFalse(whiteRook.emptyLineOfSight(new Vector2i(4, 7)));
    }

}
