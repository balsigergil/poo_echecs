package engine.pieces;

import chess.PieceType;
import chess.PlayerColor;
import engine.core.Player;
import engine.math.Vector2i;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doReturn;

public class KingTests {

    Player pl1;
    Player pl2;
    King king;

    @BeforeEach
    void setupBoard() {
        Pieces.removeAllPieces();
        pl1 = new Player(PlayerColor.WHITE);
        pl2 = new Player(PlayerColor.BLACK);
        pl1.setup();
        pl2.setup();
        king = Pieces.getKingOf(pl1);
        king.setPosition(new Vector2i(3, 3));

        for (Piece p : Pieces.getAllPiecesOfType(PieceType.PAWN)) {
            Pieces.remove(p);
        }
    }

    @DisplayName("Correct moves")
    @ParameterizedTest(name = "Correct move {index} from (3, 3) to ({0}, {1})")
    @CsvSource({
            "3, 4", "4, 3",
            "3, 2", "2, 3",
            "4, 4", "4, 2",
            "2, 2", "2, 4"
    })
    public void testCorrectMove(int x, int y) {
        assertTrue(king.checkMove(new Vector2i(x, y)));
    }

    @DisplayName("Incorrect moves")
    @ParameterizedTest(name = "Incorrect move {index} from (3, 3) to ({0}, {1})")
    @CsvSource({
            "3, 6", "6, 3",
            "3, 1", "0, 3",
            "5, 5", "5, 1",
            "1, 1", "1, 5"
    })
    public void testIncorrectMove(int x, int y) {
        assertFalse(king.checkMove(new Vector2i(x, y)));
    }

    @DisplayName("Correct kingside castling")
    @Test
    public void testKingsideCastling() {
        king.setPosition(new Vector2i(4, 0));
        Vector2i newRookPos = new Vector2i(5, 0);
        Vector2i newKingPos = new Vector2i(6, 0);

        Pieces.removePieceAt(newRookPos);
        Pieces.removePieceAt(newKingPos);

        assertTrue(king.checkMove(newKingPos));

        king.move(newKingPos);

        assertEquals(king.getPosition(), newKingPos);

        Piece rook = Pieces.getPieceAt(newRookPos);
        assertNotNull(rook);
        assertSame(PieceType.ROOK, rook.getPieceType());
    }

    @DisplayName("Correct queenside castling")
    @Test
    public void testQueensideCastling() {
        king.setPosition(new Vector2i(4, 0));
        Vector2i newRookPos = new Vector2i(3, 0);
        Vector2i newKingPos = new Vector2i(2, 0);

        Pieces.removePieceAt(new Vector2i(1, 0));
        Pieces.removePieceAt(newRookPos);
        Pieces.removePieceAt(newKingPos);

        assertTrue(king.checkMove(newKingPos));

        assertTrue(king.move(newKingPos));

        assertEquals(newKingPos, king.getPosition());

        Piece rook = Pieces.getPieceAt(newRookPos);
        assertNotNull(rook);
        assertTrue(rook instanceof Rook);
    }

    @DisplayName("Piece cause player to be in check if moved")
    @Test
    public void testInCheck() {
        Vector2i newPos = new Vector2i(3, 4);
        King spy = Mockito.spy(king);
        doReturn(false).when(spy).playerWontBeInCheck(newPos, null);
        assertFalse(spy.checkMove(newPos));
    }
}
