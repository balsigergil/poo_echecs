package engine;

import engine.core.Chessboard;
import engine.pieces.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class PlayerTests {

    @BeforeEach
    public void setup() {
        Pieces.removeAllPieces();
    }

    @Test
    @DisplayName("Test du coup du berger")
    public void testScholarsMate() {
        Chessboard.getInstance().newGame();

        Chessboard.getInstance().move(4, 1, 4, 3);
        Chessboard.getInstance().move(4, 6, 4, 4);

        Chessboard.getInstance().move(3, 0, 7, 4);
        Chessboard.getInstance().move(1, 7, 2, 5);

        Chessboard.getInstance().move(5, 0, 2, 3);
        Chessboard.getInstance().move(6, 7, 5, 5);

        Chessboard.getInstance().move(7, 4, 5, 6);

        assertTrue(Chessboard.getInstance().getPlayerBlack().isMat());
    }

    @Test
    public void testIsCheck() {
        Chessboard.getInstance().newGame();

        Chessboard.getInstance().move(3, 1, 3, 3);
        Chessboard.getInstance().move(4, 6, 4, 4);

        Chessboard.getInstance().move(3, 3, 4, 4);
        Chessboard.getInstance().move(4, 7, 4, 6);

        Chessboard.getInstance().move(2, 0, 6, 4);

        assertTrue(Chessboard.getInstance().getPlayerBlack().isCheck());
    }

    /**
     * https://fr.wikipedia.org/wiki/Pat_(%C3%A9checs)
     */
    @Test
    public void testIsPat1() {
        new King(0, 7, Chessboard.getInstance().getPlayerBlack());

        new Queen(1, 5, Chessboard.getInstance().getPlayerWhite());
        new King(2, 4, Chessboard.getInstance().getPlayerWhite());

        assertTrue(Chessboard.getInstance().getPlayerBlack().isPat());
    }

    /**
     * https://fr.wikipedia.org/wiki/Pat_(%C3%A9checs)
     */
    @Test
    public void testIsPat2() {
        new King(0, 7, Chessboard.getInstance().getPlayerBlack());

        new Pawn(0, 5, Chessboard.getInstance().getPlayerWhite());
        new Knight(2, 5, Chessboard.getInstance().getPlayerWhite());
        new King(2, 4, Chessboard.getInstance().getPlayerWhite());

        new Pawn(6, 4, Chessboard.getInstance().getPlayerWhite());
        new Pawn(7, 3, Chessboard.getInstance().getPlayerWhite());
        new Pawn(6, 5, Chessboard.getInstance().getPlayerBlack());
        new Pawn(7, 4, Chessboard.getInstance().getPlayerBlack());

        assertTrue(Chessboard.getInstance().getPlayerBlack().isPat());
    }

}
