package engine.utils;

import engine.core.Player;
import engine.pieces.Pawn;

public class PiecesUtils {

    /**
     * Ajoute des pions adverses pour tester les déplacements
     * @param player Joueur auquel ajouter les pions
     */
    public static void fillBoard(Player player) {
        for (int i = 1; i < 5; i++) {
            for (int j = 1; j < 5; j++) {
                if (i != 3 || j != 3)
                    new Pawn(i, j, player);
            }
        }
    }

}
