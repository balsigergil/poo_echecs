package engine;

import engine.core.Chessboard;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ChessboardTests {

    @Test
    public void testThreefoldRepetitionMate() {
        Chessboard.getInstance().newGame();

        Chessboard.getInstance().move(4, 1, 4, 3);
        Chessboard.getInstance().move(4, 6, 4, 4);

        Chessboard.getInstance().move(4, 0, 4, 1);
        Chessboard.getInstance().move(4, 7, 4, 6);

        Chessboard.getInstance().move(4, 1, 4, 0);
        Chessboard.getInstance().move(4, 6, 4, 7);

        Chessboard.getInstance().move(4, 0, 4, 1);
        Chessboard.getInstance().move(4, 7, 4, 6);

        Chessboard.getInstance().move(4, 1, 4, 0);
        Chessboard.getInstance().move(4, 6, 4, 7);

        assertTrue(Chessboard.getInstance().isThreefoldRepetitionMate());
    }

    @Test
    public void testNotThreefoldRepetitionMate() {
        Chessboard.getInstance().newGame();

        Chessboard.getInstance().move(4, 1, 4, 3);
        Chessboard.getInstance().move(4, 6, 4, 4);
        assertFalse(Chessboard.getInstance().isThreefoldRepetitionMate());

        Chessboard.getInstance().move(4, 0, 4, 1);
        Chessboard.getInstance().move(4, 7, 4, 6);
        assertFalse(Chessboard.getInstance().isThreefoldRepetitionMate());

        Chessboard.getInstance().move(4, 1, 4, 0);
        Chessboard.getInstance().move(4, 6, 4, 7);
        assertFalse(Chessboard.getInstance().isThreefoldRepetitionMate());
    }

}
