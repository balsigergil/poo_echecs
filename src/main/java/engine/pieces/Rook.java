package engine.pieces;

import chess.PieceType;
import engine.core.Player;
import engine.moves.MoveCapabilities;

/**
 * Classe représentant une tour
 * <p>
 * La tour peut se déplacer horizontalement et verticalement
 * de plusieurs case à la fois.
 *
 * @author Gil Balsiger
 * @author Julien Béguin
 * @see Piece
 */
public class Rook extends Piece {

    /**
     * Constructeur d'une tour précisant sa position ainsi que son propriétaire.
     *
     * @param x Coordonnée x (de 0 à 7)
     * @param y Coordonnée y (de 0 à 7)
     * @param player Joueur propriétaire
     * @see Player
     */
    public Rook(int x, int y, Player player) {
        super(x, y, PieceType.ROOK, player);
        addCapability(MoveCapabilities.VERTICAL);
        addCapability(MoveCapabilities.HORIZONTAL);
    }
}
