package engine.pieces;

import chess.PieceType;
import chess.PlayerColor;
import engine.core.Player;
import engine.moves.MoveCapabilities;

/**
 * Classe représentant une reine
 * <p>
 * La reine peut se déplacer horizontalement, verticalement et
 * en diagonale de plusieurs case à la fois.
 *
 * @author Gil Balsiger
 * @author Julien Béguin
 * @see Piece
 */
public class Queen extends Piece {

    /**
     * Constructeur plaçant automatiquement la reine en fonction de sa couleur.
     *
     * @param player Joueur propriétaire
     * @see Player
     */
    public Queen(Player player) {
        this(3, player.getColor() == PlayerColor.WHITE ? 0 : 7, player);
    }

    /**
     * Constructeur d'une reine précisant sa position ainsi que son propriétaire.
     *
     * @param x Coordonnée x (de 0 à 7)
     * @param y Coordonnée y (de 0 à 7)
     * @param player Joueur propriétaire
     * @see Player
     */
    public Queen(int x, int y, Player player) {
        super(x, y, PieceType.QUEEN, player);
        addCapability(MoveCapabilities.VERTICAL);
        addCapability(MoveCapabilities.HORIZONTAL);
        addCapability(MoveCapabilities.CROSS);
    }
}
