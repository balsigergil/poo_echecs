package engine.pieces;

import chess.PieceType;
import engine.core.Player;
import engine.moves.MoveCapabilities;

/**
 * Classe représentant un cavalier.
 * <p>
 * Le cavalier peut se déplacer soit de 2 en x et 1 en y ou
 * de 1 en x et de 2 y à chaque coup. Il possède sa propre capacité
 * de déplacement.
 *
 * @author Gil Balsiger
 * @author Julien Béguin
 * @see Piece
 */
public class Knight extends Piece {

    /**
     * Constructeur du cavalier précisant sa position ainsi que son propriétaire.
     *
     * @param x Coordonnée x (de 0 à 7)
     * @param y Coordonnée y (de 0 à 7)
     * @param player Joueur propriétaire
     * @see Player
     */
    public Knight(int x, int y, Player player) {
        super(x, y, PieceType.KNIGHT, player);
        addCapability(MoveCapabilities.KNIGHT);
    }
}
