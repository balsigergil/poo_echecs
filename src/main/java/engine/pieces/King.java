package engine.pieces;

import chess.PieceType;
import chess.PlayerColor;
import engine.core.Chessboard;
import engine.core.Player;
import engine.math.Vector2i;
import engine.moves.MoveCapabilities;
import engine.moves.PieceMove;

/**
 * Classe représentant un roi.
 * <p>
 * Le roi est une pièce qui ne peut se déplacer uniquement d'une case à la fois.
 * Il peut également effectuer un roque avec la tour lorsque toutes ces conditions
 * sont réunies:
 * <ul>
 *     <li>Le roi et la tour n'ont pas encore bougé</li>
 *     <li>Aucune pièce ne se trouve entre le roi et la tour</li>
 *     <li>Le roi n'est en échec sur aucune des case parcourues</li>
 * </ul>
 *
 * @author Gil Balsiger
 * @author Julien Béguin
 * @see Piece
 */
public class King extends Piece {

    /**
     * Constructeur plaçant automatiquement le roi en fonction de sa couleur.
     *
     * @param player Joueur propriétaire
     * @see Player
     */
    public King(Player player) {
        this(4, player.getColor() == PlayerColor.WHITE ? 0 : 7, player);
    }

    /**
     * Constructeur du roi précisant sa position ainsi que son propriétaire.
     *
     * @param x Coordonnée x (de 0 à 7)
     * @param y Coordonnée y (de 0 à 7)
     * @param player Joueur propriétaire
     * @see Player
     */
    public King(int x, int y, Player player) {
        super(x, y, PieceType.KING, player);
        addCapability(MoveCapabilities.VERTICAL);
        addCapability(MoveCapabilities.HORIZONTAL);
        addCapability(MoveCapabilities.CROSS);
        canMoveOnlyOne(true);
    }

    /**
     * Vérifie si la position indiquée est accessible par le roi
     * depuis sa position courante.
     * <p>
     * Vérifie également les cas dans lequels le roi peut effectuer
     * un petit ou grand roque.
     *
     * @param newPosition Destination du roi
     * @return <code>true</code> si le déplacement est possible en fonction des
     * capacité de la pièce, <code>false</code> sinon.
     * @see Vector2i
     */
    @Override
    public boolean checkMove(Vector2i newPosition) {
        return checkCastling(newPosition) || super.checkMove(newPosition);
    }

    /**
     * Vérifie si le roi peut se déplacer à la position indiquée grâce à ses
     * capacités. Si oui, déplace la pièce.
     * <p>
     * Vérifie également si le roi peut effectuer un roque (petit ou grand).
     * Si oui, effectue le roque.
     *
     * @param newPosition Destination du roi
     * @return <code>true</code> si le déplacement a été effectué, <code>false</code> sinon
     * @see Vector2i
     */
    @Override
    public boolean move(Vector2i newPosition) {
        if(applyCastling(newPosition)) {
            return true;
        } else {
            return super.move(newPosition);
        }
    }

    /**
     * Indique si le roi est en échec par l'autre joueur
     * en testant les déplacements des pièces de l'autre joueur
     * vers la position courante du roi.
     *
     * @return <code>true</code> si le roi est en échec, <code>false</code> sinon.
     */
    public boolean isCheck() {
        for (Piece p : Pieces.getAllPiecesFrom(Chessboard.getInstance().otherPlayer(getPlayer()))) {
            if (p.checkMove(getPosition())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Vérifie si un roque, petit ou grand, est possible en déplaçant
     * le roi à la position indiquée.
     *
     * @param newPosition Destination du roi
     * @return <code>true</code> si le roque est possible, <code>false</code> sinon.
     * @see Vector2i
     */
    private boolean checkCastling(Vector2i newPosition) {
        if (isFirstMove() && (newPosition.x == 6 || newPosition.x == 2) && newPosition.y == getY()) {

            // Sélection de la tour de droite ou de gauche en fonction du roque à effectuer
            int x = newPosition.x == 6 ? 7 : 0;
            Piece rook = Pieces.getPieceAt(new Vector2i(x, getPlayer().getColor() == PlayerColor.WHITE ? 0 : 7));

            // La pièce doit être une tour qui n'a pas bougé
            if (rook != null && rook.getPieceType() == PieceType.ROOK && rook.isFirstMove()) {
                // Aucune pièce ne se trouve entre le roi et la tour
                if(emptyLineOfSight(rook.getPosition())) {
                    return checkMove(new Vector2i(newPosition.x == 6 ? 5 : 3, getY()));
                }
            }
        }
        return false;
    }

    /**
     * Vérifie et applique le roque si celui-ci est correct
     *
     * @param newPosition Destination du roi
     * @return <code>true</code> si le roque a été appliqué, <code>false</code> sinon.
     * @see Vector2i
     */
    private boolean applyCastling(Vector2i newPosition) {
        if(checkCastling(newPosition)) {
            int x = newPosition.x == 6 ? 7 : 0;
            Piece rook = Pieces.getPieceAt(new Vector2i(x, getPlayer().getColor() == PlayerColor.WHITE ? 0 : 7));

            // Déplacement de la tour
            new PieceMove(rook, new Vector2i(x == 0 ? 3 : 5, getY())).apply();

            // Déplacement du roi
            hasMoved();
            setPosition(newPosition);

            if(x == 7)
                System.out.println("Kingside castling applied!");
            else
                System.out.println("Queenside castling applied!");

            return true;
        } else {
            return false;
        }
    }

}
