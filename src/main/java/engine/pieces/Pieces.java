package engine.pieces;

import chess.PieceType;
import engine.core.Player;
import engine.core.ViewManager;
import engine.math.Vector2i;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * Classe gérant les pièces de l'échiquier.
 * <p>
 * Cette classe permet de gérer de manière statique les pièces de l'échiquier
 * afin de permettre à l'utilisateur d'accéder aux pièces partout dans
 * l'application.
 *
 * @author Gil Balsiger
 * @author Julien Béguin
 */
public class Pieces {

    private Pieces() {
    }

    private static final List<Piece> pieces = new ArrayList<>();

    /**
     * Ajoute une pièce à l'échiquier et met à jour la vue.
     *
     * @param p Pièce à ajouter
     * @see Piece
     */
    public static void add(Piece p) {
        pieces.add(p);
        ViewManager.getInstance().addToView(p);
    }

    /**
     * Supprime une pièce de l'échiquier et met à jour la vue.
     *
     * @param p Pièce à supprimer
     * @see Piece
     */
    public static void remove(Piece p) {
        pieces.remove(p);
        ViewManager.getInstance().removeFromView(p);
    }

    /**
     * Récupére une pièce située à une position donnée.
     *
     * @param position Position de la pièce
     * @return La pièce à la position indiquée, <code>null</code>
     * si pas de pièce à cette position
     * @see Piece
     * @see Vector2i
     */
    public static Piece getPieceAt(Vector2i position) {
        for (Piece p : pieces) {
            if (p.getPosition().equals(position)) {
                return p;
            }
        }
        return null;
    }

    /**
     * Récupère les pièces d'un joueur particulier.
     *
     * @param player Joueur dont les pièces sont recherchées
     * @return Les pièces du joueur spécifié
     * @see Piece
     * @see Player
     */
    public static ArrayList<Piece> getAllPiecesFrom(Player player) {
        Objects.requireNonNull(player);
        ArrayList<Piece> result = new ArrayList<>();
        for (Piece p : pieces) {
            if (p.getPlayer() == player) {
                result.add(p);
            }
        }
        return result;
    }

    /**
     * Récupère toutes les pièces d'un type donné.
     *
     * @param type Type de pièce
     * @return Les pièces du type spécifié
     * @see Piece
     * @see PieceType
     */
    public static ArrayList<Piece> getAllPiecesOfType(PieceType type) {
        ArrayList<Piece> result = new ArrayList<>();
        for (Piece p : pieces) {
            if (p.getPieceType() == type) {
                result.add(p);
            }
        }
        return result;
    }

    /**
     * Permet de savoir si une case est vide ou non.
     *
     * @param coords
     * @return <code>true</code> si la case est vide, <code>false</code> sinon
     * @see Vector2i
     */
    public static boolean emptyCell(Vector2i coords) {
        return getPieceAt(coords) == null;
    }

    /**
     * Supprime une pièce de l'échiquier à une position donnée
     * et met à jour la vue.
     *
     * @param coords Coordonnées de la pièce à supprimer
     */
    public static void removePieceAt(Vector2i coords) {
        if (pieces.removeIf(p -> p.getPosition().equals(coords))) {
            ViewManager.getInstance().removeFromView(coords);
        }
    }

    /**
     * Supprime toutes les pièces de l'échiquier.
     */
    public static void removeAllPieces() {
        pieces.clear();
    }

    /**
     * @return Nombre de pièce(s) actuellement présente(s) sur l'échiquier
     */
    public static int getPieceCount() {
        return pieces.size();
    }

    /**
     * Retourne le {@link King roi} d'un joueur donné.
     *
     * @param player Joueur dont on veut récupérer le roi
     * @return Roi du joueur en question
     */
    public static King getKingOf(Player player) {
        for (Piece p : pieces) {
            if (p.getPlayer() == player && p.getPieceType() == PieceType.KING) {
                return (King) p;
            }
        }
        return null;
    }

    /**
     * Retourne le hash de l'échiquier permettant ensuite de déterminer
     * l'égalité entre 2 échiquier par exemple.
     * <p>
     * Fait appel à <code>hashCode</code> de chaque pièce présente sur l'échiquier.
     *
     * @return Hash de l'échiquier
     */
    public static int getBoardHash() {
        return Arrays.hashCode(pieces.toArray());
    }

}
