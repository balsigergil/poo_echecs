package engine.pieces;

import chess.PieceType;
import engine.core.Player;
import engine.moves.MoveCapabilities;

/**
 * Classe représentant un fou
 * <p>
 * Le fou peut se déplacer uniquement en diagonale.
 *
 * @author Gil Balsiger
 * @author Julien Béguin
 * @see Piece
 */
public class Bishop extends Piece {

    /**
     * Constructeur d'un fou précisant sa position ainsi que son propriétaire.
     *
     * @param x Coordonnée x (de 0 à 7)
     * @param y Coordonnée y (de 0 à 7)
     * @param player Joueur propriétaire
     * @see Player
     */
    public Bishop(int x, int y, Player player) {
        super(x, y, PieceType.BISHOP, player);
        addCapability(MoveCapabilities.CROSS);
    }
}
