package engine.pieces;

import chess.PieceType;
import engine.core.Player;
import engine.math.Vector2i;
import engine.moves.MoveCapability;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Classe représentant une pièce d'un jeu d'échec
 * <p>
 * Une pièce possède une {@link Vector2i position}, un {@link PieceType type}, un {@link Player joueur}
 * et une liste de {@link MoveCapability capacité(s) de déplacement}.
 *
 * @author Gil Balsiger
 * @author Julien Béguin
 */
public abstract class Piece {

    private final PieceType pieceType;
    private final Player player;
    private final List<MoveCapability> moveCapabilities = new ArrayList<>();

    private Vector2i position;
    private boolean firstMove = true;
    private boolean onlyOne = false;

    /**
     * Constructeur d'une pièce précisant ses coordonnées, son type ainsi que
     * son propriétaire.
     *
     * @param x         Coordonnée x (de 0 à 7)
     * @param y         Coordonnée y (de 0 à 7)
     * @param pieceType Type de le pièce
     * @param player    Joueur propriétaire de la pièce
     * @throws IllegalArgumentException Si les coordonnées indiquées sont incorrectes
     * @see PieceType
     * @see Player
     */
    public Piece(int x, int y, PieceType pieceType, Player player) throws IllegalArgumentException {
        if (x < 0 || x > 7 || y < 0 || y > 7) {
            throw new IllegalArgumentException("Piece position out of bound!");
        }

        position = new Vector2i(x, y);
        this.pieceType = pieceType;
        this.player = player;

        Pieces.add(this);
    }

    /**
     * Ajouter une capacité de déplacement à la pièce.
     * <p>
     * Permet au sous-classe d'ajouter leur(s) capacité(s).
     *
     * @param moveCapability Capacité de déplacement à ajouter
     * @see MoveCapability
     */
    protected void addCapability(MoveCapability moveCapability) {
        moveCapabilities.add(moveCapability);
    }

    /**
     * Vérification si la pièce peut se déplacer à la position indiquée en parcourant
     * ses capacités de déplacement. Vérifie également que le roi se sera pas en
     * échec après le déplacement.
     *
     * @param newPosition Destination de la pièce
     * @return <code>true</code> si le déplacement est valide, <code>false</code> sinon
     */
    public boolean checkMove(Vector2i newPosition) {
        if (newPosition == getPosition() ||
                newPosition.x > 7 ||
                newPosition.x < 0 ||
                newPosition.y > 7 ||
                newPosition.y < 0) {
            return false;
        }

        // Si pas de pièce à manger -> pieceToEat vaut null
        Piece pieceToEat = Pieces.getPieceAt(newPosition);

        // On ne peut pas se manger soi-même
        if (pieceToEat != null && pieceToEat.getPlayer().getColor() == player.getColor()) {
            return false;
        }

        return canMoveTo(newPosition) && playerWontBeInCheck(newPosition, pieceToEat);
    }

    /**
     * Vérifie les capacités de déplacement de la pièce.
     *
     * @param newPosition Destination de la pièce
     * @return <code>true</code> lors de la première capacité valide rencontrée,
     * <code>false</code> si aucune capacité n'est validée
     */
    private boolean canMoveTo(Vector2i newPosition) {
        for (MoveCapability mc : moveCapabilities)
            if (mc.verify(this, newPosition))
                return true;

        return false;
    }

    /**
     * Vérifie que le joueur n'est pas en échec si la pièce se déplace
     * à la position indiquée.
     *
     * @param newPosition Destination de la pièce
     * @param pieceToEat Pièce à retirer car mangée la pièce déplacée
     * @return <code>true</code> si le roi ne sera pas en échec,
     * <code>false</code> si il sera en échec
     */
    protected boolean playerWontBeInCheck(Vector2i newPosition, Piece pieceToEat) {
        // Si on a la possibilité de manger le roi, le déplacement est valide
        if (pieceToEat instanceof King)
            return true;

        // Sauvegrade de la piece mangée
        if (pieceToEat != null)
            Pieces.remove(pieceToEat);

        // Sauvegarde de l'ancienne position
        Vector2i oldPosition = position;
        position = newPosition;

        // Vérification de la mise en echec potentiel avec la nouvelle position
        boolean notCheck = !player.isCheck();

        // Ajout de la pièce sauvegardée et de l'ancienne position
        position = oldPosition;
        if (pieceToEat != null)
            Pieces.add(pieceToEat);

        return notCheck;
    }

    /**
     * Vérifie le déplacement et déplace la pièce à la pièce à la position indiquée.
     *
     * @param newPosition Destination de la pièce
     * @return <code>true</code> si le déplacement a été effectué, <code>false</code> sinon
     */
    public boolean move(Vector2i newPosition) {
        if (checkMove(newPosition)) {
            Pieces.removePieceAt(newPosition);
            firstMove = false;
            position = newPosition;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Permet de savoir si il n'y a pas d'autres pièces entre la position actuelle et la nouvelle position
     *
     * @param coords Nouvelle position de la pièce
     * @return <code>true</code> si la ligne de mire est dégagée, <code>false</code> si une pièce se trouve sur son chemin
     */
    public boolean emptyLineOfSight(Vector2i coords) {
        int xIncrement = Integer.compare(coords.x - position.x, 0);
        int yIncrement = Integer.compare(coords.y - position.y, 0);
        for (int x = position.x + xIncrement, y = position.y + yIncrement; x != coords.x || y != coords.y; x += xIncrement, y += yIncrement) {
            if (!Pieces.emptyCell(new Vector2i(x, y))) {
                return false;
            }
            // Empêche une boucle infinie si la fonction est mal utilisée
            if (x > 7 || y > 7 || x < 0 || y < 0)
                return false;
        }
        return true;
    }

    /**
     * @param onlyOne <code>true</code> si la pièce peut se déplacer d'une case à la fois,
     * <code>false</code> sinon
     */
    public void canMoveOnlyOne(boolean onlyOne) {
        this.onlyOne = onlyOne;
    }

    /**
     * @return <code>true</code> si la pièce peut se déplacer d'une case à la fois,
     * <code>false</code> sinon
     */
    public boolean canMoveOnlyOne() {
        return onlyOne;
    }

    public void setPosition(Vector2i coords) {
        position = coords;
    }

    public Vector2i getPosition() {
        return position;
    }

    public int getX() {
        return position.x;
    }

    public int getY() {
        return position.y;
    }

    public PieceType getPieceType() {
        return pieceType;
    }

    public Player getPlayer() {
        return player;
    }

    protected void hasMoved() {
        firstMove = false;
    }

    public boolean isFirstMove() {
        return firstMove;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(pieceType);
        sb.append(" of ");
        sb.append(player);
        sb.append(" at ");
        sb.append(getPosition());
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Piece piece = (Piece) o;
        return pieceType == piece.pieceType &&
                player.equals(piece.player) &&
                position.equals(piece.position);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pieceType, player, position);
    }
}
