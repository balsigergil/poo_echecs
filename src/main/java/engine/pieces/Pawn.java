package engine.pieces;

import chess.PieceType;
import engine.core.Chessboard;
import engine.core.Player;
import engine.choices.PieceChoice;
import engine.math.Vector2i;
import engine.moves.MoveCapabilities;
import engine.moves.MovesHistory;
import engine.moves.capabilities.PawnMoveCapability;

/**
 * Classe représentant un pion.
 * <p>
 * Le pion peut se déplacer uniquement vers l'avant de une case à la fois.
 * Il peut cependant avancer de 2 si le joueur le souhaite au 1er
 * déplacement seulement.
 * <p>
 * Le pion peut également effectuer une prise en passant et être promu en une
 * des pièces suivantes lorsque celui-ci arrive au bout de l'échiquier:
 * <ul>
 *     <li>{@link Queen Reine}</li>
 *     <li>{@link Knight Cavalier}</li>
 *     <li>{@link Bishop Fou}</li>
 *     <li>{@link Rook Tour}</li>
 * </ul>
 *
 * @author Gil Balsiger
 * @author Julien Béguin
 * @see Piece
 */
public class Pawn extends Piece {

    /**
     * Constructeur d'un pion précisant sa position ainsi que son propriétaire.
     *
     * @param x Coordonnée x (de 0 à 7)
     * @param y Coordonnée y (de 0 à 7)
     * @param player Joueur propriétaire
     * @see Player
     */
    public Pawn(int x, int y, Player player) {
        super(x, y, PieceType.PAWN, player);
        addCapability(MoveCapabilities.PAWN);
    }

    /**
     * Spécialisation du déplacement d'une pièce pour le pion.
     * Vérifie la validité du déplacement du pion ainsi que la possibilité d'une
     * promotion. Si celui-ci est valide, effectue le déplacement.
     *
     * @param newPosition Destination du pion
     * @return <code>true</code> si le déplacement a été effectué, <code>false</code> sinon.
     * @see Vector2i
     */
    @Override
    public boolean move(Vector2i newPosition) {
        // Vérification du déplacement
        if(!super.checkMove(newPosition))
            return false;

        // Prise en passant
        if (PawnMoveCapability.takePassing(getPosition(), newPosition, MovesHistory.getLastMove())) {
            Pieces.removePieceAt(new Vector2i(newPosition.x, getY()));
        } else {
            Pieces.removePieceAt(newPosition);
        }

        // Déplacement du pion
        setPosition(newPosition);
        hasMoved();

        // Promotion
        if (getY() == 0 || getY() == 7) {
            pawnPromotion();
        }
        return true;
    }

    /**
     * Promotion du pion en demandant au joueur en quelle pièce
     * promouvoir le pion.
     */
    private void pawnPromotion() {
        PieceChoice pieceChoice = Chessboard.getInstance().askUserForPawnPromotion();
        Pieces.remove(this);
        pieceChoice.instantiate(getPosition(), getPlayer());
    }
}
