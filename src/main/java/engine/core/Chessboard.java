package engine.core;

import chess.ChessController;
import chess.ChessView;
import chess.PlayerColor;
import engine.choices.*;
import engine.math.Vector2i;
import engine.moves.MovesHistory;
import engine.moves.PieceMove;
import engine.net.NetworkManager;
import engine.net.NetworkState;
import engine.pieces.Piece;
import engine.pieces.Pieces;

import java.net.InetAddress;
import java.util.HashMap;

/**
 * Classe principale de l'application gérant les partie d'échecs.
 * Représente un échiquier.
 * <p>
 * Cette classe est un contrôleur communiquant avec la {@link ChessView vue} et
 * gérant ainsi les interactions avec le/les joueur(s).
 * Elle implémente les méthodes utilisées par la vue et utilise un {@link ViewManager}
 * pour mettre à jour les informations de la partie sur la vue.
 *
 * @author Gil Balsiger
 * @author Julien Béguin
 * @see ChessController
 */
public class Chessboard implements ChessController {

    private static final boolean CHECK_TOUR = true;
    private static Chessboard instance;

    private final Player playerWhite;
    private final Player playerBlack;
    private final NetworkManager networkManager;
    private final ViewManager viewManager;
    private final HashMap<Integer, Integer> boardHashes = new HashMap<>();

    private int tour = 0;
    private boolean threeRepetition = false;

    /**
     * @return L'instance unique du Chessboard si elle existe déjà.
     * Crée une nouvelle instance sinon.
     */
    public static Chessboard getInstance() {
        if (instance == null)
            instance = new Chessboard();
        return instance;
    }

    /**
     * Constructeur privé car singleton.
     * La création se fait via {@link Chessboard#getInstance()}
     */
    private Chessboard() {
        this.networkManager = new NetworkManager(this);
        this.playerWhite = new Player(PlayerColor.WHITE);
        this.playerBlack = new Player(PlayerColor.BLACK);
        this.viewManager = ViewManager.getInstance();
    }

    /**
     * Lance la vue.
     *
     * @param view la vue à utiliser
     * @see ChessView
     */
    @Override
    public void start(ChessView view) {
        viewManager.setView(view);
        viewManager.startView();
    }

    /**
     * Appelé par la vue. Effectue un déplacement d'une pièce si celui-ci
     * est valide.
     *
     * @param fromX Coordonnées x de la pièce à déplacer
     * @param fromY Coordonnées y de la pièce à déplacer
     * @param toX Coordonnées x de destination
     * @param toY Coordonnées y de destination
     * @return <code>true</code> si le déplacement a été effectué, <code>false</code> sinon
     */
    @Override
    public boolean move(int fromX, int fromY, int toX, int toY) {
        Vector2i from = new Vector2i(fromX, fromY);
        Vector2i to = new Vector2i(toX, toY);

        Piece pieceToMove = Pieces.getPieceAt(from);

        // Vérification de la validité du déplacement et application de celui-ci si correct
        if (pieceToMove == null) {
            System.out.println("No piece selected!");
        } else if (
                CHECK_TOUR &&
                        (
                                (pieceToMove.getPlayer() == playerBlack && tour % 2 == 0) ||
                                        (pieceToMove.getPlayer() == playerWhite && tour % 2 == 1) ||
                                        (networkManager.getState() == NetworkState.HOST && tour % 2 == 1) ||
                                        (networkManager.getState() == NetworkState.CLIENT && tour % 2 == 0)
                        )
        ) {
            System.out.println("Not for you to play!");
        } else {
            PieceMove pieceMove = new PieceMove(pieceToMove, to);
            if (applyMove(pieceMove)) {
                networkManager.send(pieceMove);
                return true;
            }
        }
        return false;
    }

    /**
     * Démarrage d'une nouvelle partie en nettoyant l'échiquier préalablement.
     */
    @Override
    public void newGame() {
        tour = 0;
        cleanBoard();
        setupBoard();
    }

    /**
     * Connexion à une session de jeu existante.
     *
     * @param host Adresse de l'hôte auquel se connecter
     * @param port Port de l'hôte auquel se connecter
     * @see InetAddress
     */
    @Override
    public void connect(InetAddress host, int port) {
        networkManager.connect(host, port);
    }

    /**
     * Hébergement d'une nouvelle session de jeu.
     *
     * @param port Port sur lequel héberger la session
     */
    @Override
    public void host(int port) {
        networkManager.host(port);
    }

    /**
     * Termine le programme.
     */
    @Override
    public void onClose() {
        System.exit(0);
    }

    /**
     * Initialise les deux joueurs
     */
    public void setupBoard() {
        playerWhite.setup();
        playerBlack.setup();
    }

    /**
     * Nettoye l'échiquier, l'historique des déplacements ainsi que l'dénombrement des hash.
     */
    public void cleanBoard() {
        MovesHistory.removeAllMoves();
        Pieces.removeAllPieces();
        boardHashes.clear();
        viewManager.clearView();
    }

    /**
     * Retourne l'instance de l'autre joueur en fonction du joueur indiqué
     * <p>
     * Retoure le joueur blanc si le noir est indiqué ou le noir si le blanc est indiqué
     *
     * @param player Joueur
     * @return Autre joueur
     */
    public Player otherPlayer(Player player) {
        return player == playerWhite ? playerBlack : playerWhite;
    }

    /**
     * Appel la vue pour demander à l'utilisateur en quelle pièce
     * il souhaite promouvoir un pion.
     *
     * @return Le choix de l'utilisateur
     * @see PieceChoice
     */
    public PieceChoice askUserForPawnPromotion() {
        if (viewManager.hasView()) {
            return viewManager.getView().askUser("Promotion de pion", "En quelle pièce souhaitez-vous promouvoir ce pion ?",
                    new QueenChoice(),
                    new KnightChoice(),
                    new RookChoice(),
                    new BishopChoice());
        } else {
            // Pour les tests unitaires sans la vue
            return new QueenChoice();
        }
    }

    /**
     * Applique un déplacement de pièce si celui-ci est valide.
     *
     * @param move Déplacement à effectuer
     * @return <code>true</code> si le déplacement a été effectué, <code>false</code> sinon
     * @see PieceMove
     */
    public boolean applyMove(PieceMove move) {
        if (move.apply()) {
            checkRepetitionMate();
            updateView();
            tour++;
            return true;
        } else {
            System.out.println("Invalid move: " + move + "!");
            return false;
        }
    }

    /**
     * @return Retourne le joueur blanc
     */
    public Player getPlayerWhite() {
        return playerWhite;
    }

    /**
     * @return Retourne le joueur noir
     */
    public Player getPlayerBlack() {
        return playerBlack;
    }

    /**
     * @return <code>true</code> si l'échiquier s'est répété 3 fois, <code>false</code> sinon
     */
    public boolean isThreefoldRepetitionMate() {
        return threeRepetition;
    }

    /**
     * Vérifie le pat par répétition
     */
    private void checkRepetitionMate() {
        int boardHash = Pieces.getBoardHash();
        Integer boardCount = boardHashes.get(boardHash);

        if (boardCount == null) {
            boardHashes.put(Pieces.getBoardHash(), 1);
            threeRepetition = false;
        } else if (boardCount < 2) {
            boardHashes.put(Pieces.getBoardHash(), boardCount + 1);
            threeRepetition = false;
        } else {
            threeRepetition = true;
        }
    }

    /**
     * Met à jour la vue en fonction de l'état de la partie
     */
    private void updateView() {
        if (playerWhite.isMat()) {
            viewManager.displayMessage("Player 2 win!");
        } else if (playerBlack.isMat()) {
            viewManager.displayMessage("Player 1 win!");
        } else if (playerWhite.isPat()) {
            viewManager.displayMessage("Player 1 is pat!");
        } else if (playerBlack.isPat()) {
            viewManager.displayMessage("Player 2 is pat!");
        } else if (playerWhite.isCheck()) {
            viewManager.displayMessage("Player 1 is in check!");
        } else if (playerBlack.isCheck()) {
            viewManager.displayMessage("Player 2 is in check!");
        } else if (threeRepetition) {
            viewManager.displayMessage("3 fold repetition mate!");
        } else {
            // Nécessaire pour nettoyer la vue en mode réseau
            viewManager.displayMessage("");
        }
    }
}
