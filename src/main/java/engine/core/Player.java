package engine.core;

import chess.PlayerColor;
import engine.math.Vector2i;
import engine.pieces.*;

import java.util.Objects;

public class Player {

    private PlayerColor playerColor;

    public Player(PlayerColor playerColor) {
        this.playerColor = playerColor;
    }

    public void setup() {
        for (int i = 0; i < 8; i++) {
            new Pawn(i, playerColor == PlayerColor.WHITE ? 1 : 6, this);
        }

        new King(this);
        new Queen(this);
        int playerSide = playerColor == PlayerColor.WHITE ? 0 : 7;
        new Rook(0, playerSide, this);
        new Rook(7, playerSide, this);
        new Knight(1, playerSide, this);
        new Knight(6, playerSide, this);
        new Bishop(2, playerSide, this);
        new Bishop(5, playerSide, this);
    }

    public boolean isCheck() {
        King king = Pieces.getKingOf(this);
        if(king == null) {
            return false;
        } else {
            return king.isCheck();
        }
    }

    public boolean cannotMove() {
        for (Piece p : Pieces.getAllPiecesFrom(this)) {
            for (int i = 0; i < 8; i++) {
                for (int j = 0; j < 8; j++) {
                    if (p.checkMove(new Vector2i(i, j))) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public boolean isMat() {
        return cannotMove() && isCheck();
    }

    public boolean isPat() {
        return cannotMove() && !isCheck();
    }

    public PlayerColor getColor() {
        return playerColor;
    }

    @Override
    public String toString() {
        switch (playerColor) {
            case WHITE:
                return "Player 1 (White)";
            case BLACK:
                return "Player 2 (Black)";
            default:
                return "Unknown player";
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return playerColor == player.playerColor;
    }

    @Override
    public int hashCode() {
        return Objects.hash(playerColor);
    }
}
