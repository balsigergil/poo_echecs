package engine.core;

import chess.ChessView;
import engine.math.Vector2i;
import engine.pieces.Piece;
import engine.pieces.Pieces;

/**
 * Classe singleton gérant la vue
 * <p>
 * La plupart des méthodes ne font rien si la vue est null
 * car les tests unitaire sont executés sans vue.
 *
 * @author Gil Balsiger
 * @author Julien Béguin
 * @see ChessView
 */
public class ViewManager {

    private static ViewManager instance;

    private ChessView chessView;

    public static ViewManager getInstance() {
        if (instance == null)
            instance = new ViewManager();
        return instance;
    }

    private ViewManager() {
    }

    public void startView() {
        chessView.startView();
    }

    public void addToView(Piece p) {
        if (hasView()) {
            chessView.putPiece(p.getPieceType(), p.getPlayer().getColor(), p.getX(), p.getY());
        }
    }

    public void removeFromView(Piece p) {
        if (hasView()) {
            chessView.removePiece(p.getX(), p.getY());
        }
    }

    public void removeFromView(Vector2i coord) {
        if (hasView()) {
            chessView.removePiece(coord.x, coord.y);
        }
    }

    public void moveInView(Vector2i fromPosition, Vector2i newPosition) {
        if (hasView()) {
            removeFromView(fromPosition);

            // On récupère à nouveau la pièce car celle-ci aurait pu change dans le cas d'une promotion
            Piece p = Pieces.getPieceAt(newPosition);

            if (p != null)
                chessView.putPiece(p.getPieceType(), p.getPlayer().getColor(), newPosition.x, newPosition.y);
        }
    }

    public void displayMessage(String message) {
        if (hasView()) {
            chessView.displayMessage(message);
        }
    }

    public void clearView() {
        if (hasView()) {
            for (int i = 0; i < 8; i++) {
                for (int j = 0; j < 8; j++) {
                    chessView.removePiece(i, j);
                }
            }
        }
    }

    public void setView(ChessView chessView) {
        this.chessView = chessView;
    }

    public ChessView getView() {
        return chessView;
    }

    public boolean hasView() {
        return chessView != null;
    }

}
