package engine.net;

import engine.core.Chessboard;
import engine.core.ViewManager;
import engine.moves.PieceMove;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Objects;

public class NetworkManager {

    private final Chessboard chessboard;
    private NetworkState state;
    private Socket socket;
    private Thread netThread;

    public NetworkManager(Chessboard chessboard) {
        Objects.requireNonNull(chessboard);
        this.chessboard = chessboard;
        this.state = NetworkState.DISCONNECTED;
    }

    public void host(int port) {
        chessboard.cleanBoard();
        netThread = new Thread(() -> {
            System.out.println("Hosting on " + port + "...");
            try {
                ServerSocket ss = new ServerSocket(port);
                ViewManager.getInstance().displayMessage("Waiting for player...");
                socket = ss.accept();
                state = NetworkState.HOST;
                System.out.println("[SERVER] Client " + socket.getInetAddress() + " connected");
                chessboard.newGame();
                readMoves();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        netThread.start();
    }

    public void connect(InetAddress host, int port) {
        netThread = new Thread(() -> {
            System.out.println("[CLIENT] Connecting to " + host + ":" + port + "...");
            try {
                socket = new Socket(host, port);
                state = NetworkState.CLIENT;
                System.out.println("[CLIENT] Connected to " + socket.getInetAddress());
                chessboard.newGame();
                readMoves();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        netThread.start();
    }

    public void send(PieceMove pieceMove) {
        if(socket != null) {
            try {
                ObjectOutputStream oout = new ObjectOutputStream(socket.getOutputStream());
                oout.writeObject(pieceMove);
                oout.flush();
            } catch (IOException e) {
                System.out.println("Disconnected!");
                close();
            }
        }
    }

    public void close() {
        try {
            if(socket != null) {
                socket.close();
                socket = null;
                state = NetworkState.DISCONNECTED;
                System.out.println("Socket closed");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void readMoves() {
        while (true) {
            try {
                ObjectInputStream oin = new ObjectInputStream(socket.getInputStream());
                PieceMove move = (PieceMove) oin.readObject();
                chessboard.applyMove(move);
            } catch (IOException | ClassNotFoundException e) {
                System.out.println("Disconnected!");
                close();
                return;
            }
        }
    }

    public NetworkState getState() {
        return state;
    }
}
