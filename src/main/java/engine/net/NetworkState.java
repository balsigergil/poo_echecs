package engine.net;

public enum NetworkState {
    CLIENT, HOST, DISCONNECTED;
}
