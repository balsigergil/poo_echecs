package engine.choices;

import engine.core.Player;
import engine.math.Vector2i;
import engine.pieces.Knight;

public class KnightChoice extends PieceChoice {
    @Override
    public String textValue() {
        return "Knight";
    }

    @Override
    public void instantiate(Vector2i position, Player owner) {
        new Knight(position.x, position.y, owner);
    }
}
