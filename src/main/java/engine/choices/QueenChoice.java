package engine.choices;

import engine.core.Player;
import engine.math.Vector2i;
import engine.pieces.Queen;

public class QueenChoice extends PieceChoice {
    @Override
    public String textValue() {
        return "Queen";
    }

    @Override
    public void instantiate(Vector2i position, Player owner) {
        new Queen(position.x, position.y, owner);
    }
}
