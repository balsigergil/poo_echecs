package engine.choices;

import engine.core.Player;
import engine.math.Vector2i;
import engine.pieces.Rook;

public class RookChoice extends PieceChoice {
    @Override
    public String textValue() {
        return "Rook";
    }

    @Override
    public void instantiate(Vector2i position, Player owner) {
        new Rook(position.x, position.y, owner);
    }
}
