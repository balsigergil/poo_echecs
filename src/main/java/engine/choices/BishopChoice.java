package engine.choices;

import engine.core.Player;
import engine.math.Vector2i;
import engine.pieces.Bishop;

public class BishopChoice extends PieceChoice {
    @Override
    public String textValue() {
        return "Bishop";
    }

    @Override
    public void instantiate(Vector2i position, Player owner) {
        new Bishop(position.x, position.y, owner);
    }
}
