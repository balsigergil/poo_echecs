package engine.choices;

import chess.ChessView.UserChoice;
import engine.core.Player;
import engine.math.Vector2i;

public abstract class PieceChoice implements UserChoice {

    public abstract void instantiate(Vector2i position, Player owner);

    @Override
    public String toString() {
        return textValue();
    }

}
