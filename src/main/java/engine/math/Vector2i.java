package engine.math;

import java.io.Serializable;
import java.util.Objects;

public class Vector2i implements Serializable {

    public int x, y;

    public Vector2i() {
        x = 0;
        y = 0;
    }

    public Vector2i(Vector2i position) {
        x = position.x;
        y = position.y;
    }

    public Vector2i(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "[" + (char) ('A' + x) + "," + (y + 1) + "]";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vector2i vector2i = (Vector2i) o;
        return x == vector2i.x && y == vector2i.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }
}
