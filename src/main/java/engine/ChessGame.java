package engine;

import chess.views.gui.GUIView;
import engine.core.Chessboard;

public class ChessGame {

    public static void main(String[] args) {
        Chessboard chess = Chessboard.getInstance();
//        ConsoleView cv = new ConsoleView(chess);
        GUIView cv = new GUIView(chess);
        chess.start(cv);
    }
}
