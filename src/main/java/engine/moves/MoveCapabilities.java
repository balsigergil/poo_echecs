package engine.moves;

import engine.moves.capabilities.*;

public class MoveCapabilities {

    public static final MoveCapability VERTICAL = new VerticalMoveCapability();
    public static final MoveCapability HORIZONTAL = new HorizontalMoveCapability();
    public static final MoveCapability CROSS = new CrossMoveCapability();
    public static final MoveCapability KNIGHT = new KnightMoveCapability();
    public static final MoveCapability PAWN = new PawnMoveCapability();

}
