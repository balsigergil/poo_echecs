package engine.moves;

import engine.core.ViewManager;
import engine.math.Vector2i;
import engine.pieces.Piece;
import engine.pieces.Pieces;

import java.io.Serializable;

public class PieceMove implements Serializable {

    private transient Piece piece;

    private Vector2i fromPosition;
    private Vector2i toPosition;

    public PieceMove(Piece piece, Vector2i toPosition) {
        this.piece = piece;
        this.fromPosition = piece.getPosition();
        this.toPosition = toPosition;
    }

    private boolean validate() {
        if (piece == null) {
            piece = Pieces.getPieceAt(fromPosition);
        }

        if (piece != null) {
            if (piece.getPosition().equals(fromPosition)) {
                return true;
            } else {
                System.out.println("Invalid move: piece has already been moved!");
                return false;
            }
        } else {
            throw new NullPointerException("Error validating move: Piece not set!");
        }
    }

    public boolean apply() {
        if (validate() && piece.move(toPosition)) {
            ViewManager.getInstance().moveInView(fromPosition, toPosition);
            MovesHistory.registerMove(this);
            System.out.println("Move applied: " + this);
            return true;
        } else {
            return false;
        }
    }

    public Vector2i getFromPosition() {
        return fromPosition;
    }

    public Vector2i getToPosition() {
        return toPosition;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if(piece != null) {
            sb.append(piece.getPieceType());
            sb.append(" of ");
            sb.append(piece.getPlayer());
            sb.append(" ");
        }
        sb.append("from ").append(fromPosition).append(" to ").append(toPosition);
        return sb.toString();
    }

}
