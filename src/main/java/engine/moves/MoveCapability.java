package engine.moves;

import engine.math.Vector2i;
import engine.pieces.Piece;

public interface MoveCapability {

    boolean verify(Piece piece, Vector2i newPosition);

}
