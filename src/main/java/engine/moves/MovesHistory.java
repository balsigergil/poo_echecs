package engine.moves;

import java.util.EmptyStackException;
import java.util.Stack;

public class MovesHistory {

    private static final Stack<PieceMove> pieceMoves = new Stack<>();

    private MovesHistory() {
    }

    public static void removeAllMoves() {
        pieceMoves.clear();
    }

    public static void registerMove(PieceMove move) {
        if (move != null)
            pieceMoves.push(move);
    }

    public static PieceMove getLastMove() {
        try {
            return pieceMoves.peek();
        } catch (EmptyStackException e) {
            return null;
        }
    }

}
