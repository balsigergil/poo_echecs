package engine.moves.capabilities;

import engine.moves.MoveCapability;
import engine.math.Vector2i;
import engine.pieces.Piece;

public class HorizontalMoveCapability implements MoveCapability {

    @Override
    public boolean verify(Piece piece, Vector2i newPosition) {
        boolean canMove = false;
        if (newPosition.y == piece.getY()) {
            canMove = piece.emptyLineOfSight(newPosition);
            if (canMove && piece.canMoveOnlyOne()) {
                canMove = Math.abs(piece.getX() - newPosition.x) == 1;
            }
        }
        return canMove;
    }

}
