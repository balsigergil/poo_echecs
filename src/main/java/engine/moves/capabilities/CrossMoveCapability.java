package engine.moves.capabilities;

import engine.moves.MoveCapability;
import engine.math.Vector2i;
import engine.pieces.Piece;

public class CrossMoveCapability implements MoveCapability {

    @Override
    public boolean verify(Piece piece, Vector2i newPosition) {
        boolean canMove = false;
        if (Math.abs(newPosition.x - piece.getX()) == Math.abs(newPosition.y - piece.getY())) {
            canMove = piece.emptyLineOfSight(newPosition);
            if (canMove && piece.canMoveOnlyOne()) {
                canMove = Math.abs(piece.getY() - newPosition.y) == 1 && Math.abs(piece.getX() - newPosition.x) == 1;
            }
        }
        return canMove;
    }

}
