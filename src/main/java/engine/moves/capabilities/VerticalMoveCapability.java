package engine.moves.capabilities;

import engine.moves.MoveCapability;
import engine.math.Vector2i;
import engine.pieces.Piece;

public class VerticalMoveCapability implements MoveCapability {

    @Override
    public boolean verify(Piece piece, Vector2i newPosition) {
        boolean canMove = false;
        if (newPosition.x == piece.getX()) {
            canMove = piece.emptyLineOfSight(newPosition);
            if (canMove && piece.canMoveOnlyOne()) {
                canMove = Math.abs(piece.getY() - newPosition.y) == 1;
            }
        }
        return canMove;
    }

}
