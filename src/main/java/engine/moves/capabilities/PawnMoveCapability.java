package engine.moves.capabilities;

import chess.PieceType;
import chess.PlayerColor;
import engine.math.Vector2i;
import engine.moves.MoveCapability;
import engine.moves.MovesHistory;
import engine.moves.PieceMove;
import engine.pieces.Piece;
import engine.pieces.Pieces;

public class PawnMoveCapability implements MoveCapability {

    @Override
    public boolean verify(Piece piece, Vector2i newPosition) {

        boolean moveVertical =
                moveForward(piece.getPosition(), newPosition) &&
                newPosition.x == piece.getPosition().x &&
                Pieces.emptyCell(newPosition);

        boolean moveVerticalOne =
                moveVertical &&
                Math.abs(piece.getY() - newPosition.y) == 1;

        // Le pion peut se déplacer de 2 au premier déplacement si toutes les cases ou il passe sont vide
        boolean moveVerticalTwo =
                moveVertical &&
                piece.isFirstMove() &&
                piece.emptyLineOfSight(newPosition) &&
                Math.abs(piece.getY() - newPosition.y) == 2;

        // Prise en diagonale
        boolean takeDiagonal =
                moveDiagonal(piece.getPosition(), newPosition) &&
                !Pieces.emptyCell(newPosition) &&
                Pieces.getPieceAt(newPosition).getPlayer().getColor() != Pieces.getPieceAt(piece.getPosition()).getPlayer().getColor();

        return moveVerticalOne ||
                moveVerticalTwo ||
                takeDiagonal ||
                takePassing(piece.getPosition(), newPosition, MovesHistory.getLastMove());
    }

    public static boolean takePassing(Vector2i currentPosition, Vector2i newPosition, PieceMove lastMove) {
        Vector2i adjacentPos = new Vector2i(newPosition.x, currentPosition.y);
        return moveDiagonal(currentPosition, newPosition) &&
                Pieces.emptyCell(newPosition) &&
                !Pieces.emptyCell(adjacentPos) &&
                lastMove != null &&
                lastMove.getToPosition().equals(adjacentPos) &&
                (lastMove.getFromPosition().y == 1 || lastMove.getFromPosition().y == 6) &&
                (lastMove.getToPosition().y == 3 || lastMove.getToPosition().y == 4) &&
                Pieces.getPieceAt(adjacentPos).getPieceType() == PieceType.PAWN;
    }

    private static boolean moveForward(Vector2i currentPosition, Vector2i newPosition) {
        return !(
                (Pieces.getPieceAt(currentPosition).getPlayer().getColor() == PlayerColor.WHITE && newPosition.y - currentPosition.y < 0) ||
                (Pieces.getPieceAt(currentPosition).getPlayer().getColor() == PlayerColor.BLACK && newPosition.y - currentPosition.y > 0)
        );
    }

    private static boolean moveDiagonal(Vector2i currentPosition, Vector2i newPosition) {
        return moveForward(currentPosition, newPosition) &&
                Math.abs(currentPosition.y - newPosition.y) == 1 &&
                Math.abs(currentPosition.x - newPosition.x) == 1;
    }

}
