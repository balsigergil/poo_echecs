package engine.moves.capabilities;

import engine.moves.MoveCapability;
import engine.math.Vector2i;
import engine.pieces.Piece;

public class KnightMoveCapability implements MoveCapability {

    @Override
    public boolean verify(Piece piece, Vector2i newPosition) {
        return (Math.abs(newPosition.x - piece.getX()) == 1 && Math.abs(newPosition.y - piece.getY()) == 2) ||
                (Math.abs(newPosition.x - piece.getX()) == 2 && Math.abs(newPosition.y - piece.getY()) == 1);
    }

}
